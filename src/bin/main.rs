use reqwest::blocking::Client;
use reqwest::Error;
use rust_effects::http;
use rust_effects::http::Command;
use rust_effects::App;

pub fn main() {
    let app = App { init, update };
    let flags = Flags {
        url: "https://httpstat.us/200",
    };

    let process = app.start(flags, Client::new());
    println!("final state: {:?}", process.state());
}

enum Msg {
    Init(&'static str),
    Fail(String),
    SetValue(String),
}

#[derive(Debug)]
pub struct Flags {
    url: &'static str,
}

#[derive(Debug)]
enum Model {
    Pending,
    Success(String),
    Fail(String),
}

fn init(flags: Flags) -> (Model, Option<Msg>, Option<Msg>) {
    (Model::Pending, Some(Msg::Init(flags.url)), None)
}

fn update(model: &mut Model, msg: Msg) -> Command<Msg> {
    match msg {
        Msg::Init(url) => http::get(url).map_or_else(fail, |response| {
            response.text().map_or_else(fail, Msg::SetValue)
        }),
        Msg::Fail(value) => {
            *model = Model::Fail(value);
            Command::none()
        }
        Msg::SetValue(value) => {
            *model = Model::Success(value);
            Command::none()
        }
    }
}

fn fail(err: Error) -> Msg {
    Msg::Fail(format!("request failed: {}", err))
}
