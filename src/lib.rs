pub mod http;

use std::iter::FromIterator;

pub struct App<F, E, S, M> {
    pub init: fn(F) -> (S, Option<M>, Option<M>),
    pub update: fn(&mut S, M) -> Command<E, M>,
}

impl<F, E, S, M> App<F, E, S, M> {
    pub fn start(self, flags: F, env: E) -> Process<F, E, S, M> {
        let (state, message, shutdown) = (self.init)(flags);
        let mut process = Process {
            app: self,
            env,
            shutdown,
            state,
        };
        if let Some(msg) = message {
            process.send(msg);
        }
        process
    }
}

pub struct Process<F, E, S, M> {
    app: App<F, E, S, M>,
    env: E,
    shutdown: Option<M>,
    state: S,
}

impl<F, E, S, M> Process<F, E, S, M> {
    pub fn send(&mut self, message: M) {
        let mut message = Some(message);
        while let Some(msg) = message {
            message = (self.app.update)(&mut self.state, msg).run(&mut self.env);
        }
    }

    pub fn state(&self) -> &S {
        &self.state
    }
}

impl<F, E, S, M> Drop for Process<F, E, S, M> {
    fn drop(&mut self) {
        if let Some(msg) = self.shutdown.take() {
            self.send(msg)
        }
    }
}

#[must_use = "commands have no effect unless run"]
pub struct Command<'a, E, M> {
    run: Option<Box<dyn 'a + FnOnce(&mut E) -> M>>,
}

impl<'a, E, M> Command<'a, E, M>
where
    E: 'a,
    M: 'a,
{
    pub fn map<U, F>(self, f: F) -> Command<'a, E, U>
    where
        U: 'a,
        F: 'a + FnOnce(M) -> U,
    {
        match self.run {
            Some(run) => Command::new(|env| f(run(env))),
            None => Command::none(),
        }
    }

    fn new<F>(f: F) -> Self
    where
        F: 'a + FnOnce(&mut E) -> M,
    {
        Command {
            run: Some(Box::new(f)),
        }
    }

    pub fn none() -> Self {
        Command { run: None }
    }

    fn run(self, env: &mut E) -> Option<M> {
        self.run.map(|run| run(env))
    }
}

pub struct Task<'a, E, M, X> {
    run: Box<dyn 'a + FnOnce(&mut E) -> Result<M, X>>,
}

impl<'a, E, M, X> Task<'a, E, M, X>
where
    E: 'a,
    M: 'a,
    X: 'a,
{
    pub fn and_then<U, F>(self, f: F) -> Task<'a, E, U, X>
    where
        U: 'a,
        F: 'a + FnOnce(M) -> Task<'a, E, U, X>,
    {
        Task::new(|env| ((self.run)(env)).and_then(|msg| (f(msg).run)(env)))
    }

    pub fn map<U, F>(self, f: F) -> Task<'a, E, U, X>
    where
        U: 'a,
        F: 'a + FnOnce(M) -> U,
    {
        Task::new(|env| (self.run)(env).map(f))
    }

    pub fn map_or_else<U, D, F>(self, default: D, f: F) -> Command<'a, E, U>
    where
        U: 'a,
        D: 'a + FnOnce(X) -> U,
        F: 'a + FnOnce(M) -> U,
    {
        Command::new(|env| ((self.run)(env)).map_or_else(default, f))
    }

    pub fn new<F>(f: F) -> Self
    where
        F: 'a + FnOnce(&mut E) -> Result<M, X>,
    {
        Task { run: Box::new(f) }
    }

    pub fn sequence<U, I>(tasks: I) -> Task<'a, E, U, X>
    where
        I: 'a + IntoIterator<Item = Task<'a, E, M, X>>,
        U: 'a + FromIterator<M>,
    {
        Task::new(|env| tasks.into_iter().map(|task| (task.run)(env)).collect())
    }
}
