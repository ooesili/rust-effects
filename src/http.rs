use reqwest::blocking::{Client, Response};
use reqwest::{Error, IntoUrl};

pub type Command<'a, T> = super::Command<'a, Client, T>;
pub type Task<'a, T> = super::Task<'a, Client, T, Error>;

pub fn get<'a, U>(url: U) -> Task<'a, Response>
where
    U: 'a + IntoUrl,
{
    Task {
        run: Box::new(move |client| client.get(url).send()),
    }
}
